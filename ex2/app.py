from bs4 import BeautifulSoup
from .utils import find_json


def analyze_html(html_doc: str):
    supported_formats = {"backstage": {"tag": "meta", "query": {"property": "og:site_name"},
                                       "attr": "content", "handler": backstage},
                        "indeed": {"tag": "meta", "query": {"property": "og:site_name"},
                                       "attr": "content", "handler": indeed},
                        "linkedin": {"tag": "meta", "query": {"name": "application-name"},
                                       "attr": "content", "handler": linkedin}}
    soup = BeautifulSoup(html_doc, 'html.parser')
    doc_format = ""
    for k, v in supported_formats.items():
        doc_format = soup.find(v["tag"], {**v['query']})
        if doc_format and k in doc_format[v["attr"]].lower():
            doc_format = k
            break
    if not doc_format:
        return "This document format is not supported"
    return supported_formats[doc_format]['handler'](soup)


def backstage(soup):
    # f_name, l_name = soup.find("meta", property="og:title")['content'].split(" ", 1)
    cuid = [i for i in soup.find_all("script") if "var CUID =" in i.text][0]
    cuid_list = [var for var in cuid.text.split('\n') if "=" in var]
    cuid_dict = {var_name.replace(" ", ""): value for var_name, value in [var.split("=", 1) for var in cuid_list]}
    profile_data = find_json(cuid_dict["profile_data"])
    if not profile_data:
        raise UserWarning("error ocured during parsing the document")
    f_name, l_name = profile_data['display_name'].split(" ", 1)
    skills = [skill['name'] for skill in profile_data['skills']]
    education_data = find_json(cuid_dict["education_data"])
    education = [{"degree": edc["degree"], "school": edc["school"]} for edc in education_data]
    return {
        "First name": f_name,
        "Last name": l_name,
        "Skills": skills,
        "Education": education
    }

def indeed(soup):
    education_items = soup.find("div", id="education-items")
    education_sections = education_items.find_all("div", {"class":"education-section"})
    # print(education_sections)
    education = []
    for section in education_sections:
        education.append({
            "degree": section.find("p", {"class": "edu_title"}).text,
            "school": section.find("div", {"class": "edu_school"}).text
        })

    skills_items = soup.find("div", id="skills-items")
    additionalinfo_items = soup.find("div", id="additionalinfo-items")
    skills = []
    if skills_items:
        skills.append(skills_items.find("span" ,{"class": "skill-text"}).text.split(","))
    elif additionalinfo_items:
        additionalinfo_sections = additionalinfo_items.find_all("div", id="additionalinfo-section")
        for section in additionalinfo_sections:
            skills.append(section.find("div", {"class":"data_display"}).text)
    
    return {
        "First name": '',
        "Last name": '',
        "Skills": skills,
        "Education": education
    }

def linkedin(soup):
    full_name = soup.find("title").text.split("|")[0]
    f_name, l_name = full_name.split(" ", 1)
    background_skills_container = soup.find("div", id="background-skills-container")
    if background_skills_container:
        skills_items = background_skills_container.find_all("li", {"class": "endorse-item"})
        skills = [skill.text for skill in skills_items]
    else:
        skills = []
    background_education_container = soup.find("div", id="background-education-container")
    if background_education_container:
        education_items = background_education_container.find_all("div", {"class":"education"})
        education = [{"degree": edc.find("h5").text, "school":edc.find("h4").text} for edc in education_items]
    else:
        education = []
    return {
        "First name": f_name,
        "Last name": l_name,
        "Skills": skills,
        "Education": education
    }



if __name__ == "__main__":
    with open(r"/home/leonid/interviews/pipl_interview/ex2/samples/linkedin/1003_172bc83cb13a5022.html", 'r') as fin:
        html_doc = fin.read()
    # print(analyze_html(html_doc))
    for k, v in analyze_html(html_doc).items():
        print("{} == {}".format(k, v))
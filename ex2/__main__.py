import sys
from .app import analyze_html

html_path = sys.argv[1]
if not html_path:
    exit("html document path is not passed")

with open(html_path, "r") as fin:
    html_doc = fin.read()

res = analyze_html(html_doc)
if isinstance(res, dict):
    for k, v in res.items():
        print("{} = {}".format(k, v))
else:
    print(res)

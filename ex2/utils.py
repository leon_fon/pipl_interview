import json

def find_json(data: str):
    """
    Try to find first json in passed string
    :param data: text where json can be
    :return: parsed json to dict
    :rtype: dict
    """

    brackets = {"{": "}", "[": "]"}
    for i in range(len(data)):
        if data[i] in brackets:
            start_i, open_bracket, close_bracket = i, data[i], brackets[data[i]]
            break
    else:
        return False

    b_cout = 0
    for i in range(start_i, len(data)):
        if data[i] == open_bracket:
            b_cout += 1
        elif data[i] == close_bracket:
            b_cout -= 1
        
        if b_cout == 0:
            return json.loads(data[:i+1])
        if b_cout < 0:
            return False
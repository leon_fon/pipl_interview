
from mongoengine import *


class Employee(Document):
    """
    Employee representation class
    """
    employee_id = IntField(precision=3, unique=True)
    first_name = StringField(required=True)
    last_name = StringField(required=True)
    address = StringField(required=True)
    departament = StringField(required=True)
    manager = IntField()

    meta = {'allow_inheritance': True}

    def __str__(self):
        return str(self.employee_id)

    @staticmethod
    def remove_employee(employee_id: int):
        """
        Remove employee from DB by passed employee_id
        :param employee_id: employee ID
        :return: suitable message 
        """
        try:
            employee = Employee.objects.get(employee_id=employee_id)
        except Employee.DoesNotExist:
                return "There is no employee with this employee_id ({})".format(employee_id)
        if employee.employees:
            for empl in employee.employees:
                empl.manager = None
                empl.save()
        employee.delete()
        return "Employee with employee_id {} deleted".format(employee_id)
    
    @staticmethod
    def get_employee(employee_id: int):
        """
        Returns's employee data by passed employee_id
        :param employee_id: employee ID
        :return: dict with all available information of the employee
        """
        try:
            employee = Employee.objects.get(employee_id=employee_id)
        except Employee.DoesNotExist:
            return "Employee with this employee_id ({}) is not exist".format(employee_id)
        return {k: employee[k] for k in employee if not k.startswith('_') and k != 'employee_id'}

    @staticmethod
    def get_common_manager(id_1: int, id_2: int):
        """
        Returns first common manager for two employees, which manages them directly or indirectly both
        :param id_1: First employee ID
        :param id_2: Second employee ID
        :return: First common manager information
        """
        try:
            employee_1 = Employee.objects.get(employee_id=id_1)
        except Employee.DoesNotExist:
            return "Employee with employee_id {} not exists".format(id_1)

        try:
            employee_2 = Employee.objects.get(employee_id=id_2)
        except Employee.DoesNotExist:
            return "Employee with employee_id {} not exists".format(id_2)

        if not (employee_1.manager and employee_2.manager):
            return "There is no common manger"

        managers_list_1 = []
         
        manager_1 = Manager.objects.get(employee_id=employee_1.manager)
        managers_list_1.append(manager_1)
        while True:
            if employee_2 in manager_1.employees:
                return Manager.get_employee(manager_1.employee_id)
            if not manager_1.manager:
                break
            manager_1 = Manager.objects.get(employee_id=manager_1.manager)
            managers_list_1.append(manager_1)
        
        manager_2 = Manager.objects.get(employee_id=employee_2.manager)
        while True:
            if manager_2 in managers_list_1:
                return Manager.get_employee(manager_2.employee_id)
            if not manager_2.manager:
                return "There is no common manger"
            manager_2 = Manager.objects.get(employee_id=manager_2.manager)


class Manager(Employee):
    """
    Manager representation class
    """
    employees = ListField(ReferenceField(Employee))

    @staticmethod
    def add_to_manger(manager_id: int, employee_id: int):
        """
        Add employee to manager
        :param manager_id: manager employee ID
        :param employee_id: employee ID
        :return: suitable message
        """
        try:
            manager = Manager.objects.get(employee_id=manager_id)
        except Employee.DoesNotExist:
            return "Employee with employee_id {} not exists".format(manager_id)

        try:
            employee = Employee.objects.get(employee_id=employee_id)
        except Employee.DoesNotExist:
            return "Employee with employee_id {} not exists".format(manager_id)

        if employee.manager:
            return "This employee (employee_id = {}) already has manager".format(employee_id)
        if not manager.employees:
            manager.employees = [employee]
            employee.manager = manager.employee_id
        else:
            manager.employees.append(employee)
            manager.employees = list(set(manager.employees))
            employee.manager = manager.employee_id
        manager.save()
        employee.save()
        return "Employee {} is added to manger {}".format(employee, manager)


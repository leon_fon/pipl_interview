import sys
import yaml
import os
from .models import Employee, Manager
from mongoengine import connect

current_dir = os.path.dirname(os.path.realpath(__file__))

with open(os.path.join(current_dir, "config.yml"), "r") as fin:
    config = yaml.load(fin.read())
db_config = config["db_configuration"]

connect(db_config["db_name"], host=db_config["db_host"], port=db_config["db_port"])


def fill_sample_data():
    Employee(123, "Alex", "Davidson", "Petah Tikva", "IT").save()
    Employee(321, "Tyler", "Hamilton", "Petah Tikva", "IT").save()
    Employee(654, "Sidney", "Fisher", "Petah Tikva", "RD").save()
    Manager(741, "Bev", "Brooks", "Petah Tikva", "Sales").save()
    Manager(852, "Jody", "Saunders", "Petah Tikva", "RD").save()
    Manager(789, "Jessie", "Moran", "Petah Tikva", "RD").save()
    Manager(963, "Mel", "Ellis", "Petah Tikva", "The Big Boss").save()
    print("Sample data is filled!")


def test():
    print("\n{d}\nTest add to manager\n{d}\n".format(d="-*" * 10))
    print(Manager.add_to_manger(963, 789))
    print(Manager.add_to_manger(789, 852))
    print(Manager.add_to_manger(789, 741))
    print(Manager.add_to_manger(852, 654))
    print(Manager.add_to_manger(852, 321))
    print(Manager.add_to_manger(741, 123))

    print("\n{d}\nTest get common manager\n{d}\n".format(d="-*" * 10))
    message = "Common manger for ids {} and {} is {}"
    print(message.format(654, 123, Employee.get_common_manager(654, 123)))
    print(message.format(789, 123, Employee.get_common_manager(789, 123)))
    print(message.format(741, 852, Employee.get_common_manager(741, 852)))
    print(message.format(654, 321, Employee.get_common_manager(654, 321)))
    print(message.format(741, 789, Employee.get_common_manager(741, 789)))
    print(message.format(852, 789, Employee.get_common_manager(852, 789)))
    print(message.format(852, 963, Employee.get_common_manager(852, 963)))

    print("\n{d}\nTest get employee data and remove employee\n{d}\n".format(d="-*"*10))
    print(Manager.get_employee(852))
    print(Manager.remove_employee(852))
    print(Manager.get_employee(852))




for arg in sys.argv[1:]:
    try:
        getattr(sys.modules[__name__], arg)()
    except AttributeError:
        print("Option {} is not supported".format(arg))

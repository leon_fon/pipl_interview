# PIPL home assighn for new candidate


### Prerequisites

Python 3.6.x shuld be installed

Running instanmce of MongoDB

### Installing

Need to install all requirements from requirements.txt file

```bash
cd <project-root>
pip install -r requirements.txt d
```

Configure the DB configuration file

```bash
cd <project-root>/ex1
nano config.yml

# config example:
# db_configuration:
#   db_name: pipl
#   db_host: localhost
#   db_port: 27017
```

## Test the applications

Exercise 1 (manager and employee structures):


```bash
cd <project-root>

python -m ex1 fill_sample_data
python -m ex1 test
```

Exercise 2 (html scraping; find full name, education and skills of person):

```
cd <project-root>

python -m ex2 <path to html file>
```

## Author

Leonid Fonaryov Rubin
